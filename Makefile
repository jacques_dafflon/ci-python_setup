ifndef WORKSPACE
	WORKSPACE:=$(CURDIR)
	MSG:="Setting WORKSPACE to: ${WORKSPACE}"
else
	MSG:="WORKSPACE already set to: ${WORKSPACE}"
endif

ifdef BUILD_NUMBER
	CACHE_DIR:="/var/lib/jenkins/pip-cache"
else
	CACHE_DIR:="${WORKSPACE}/.pip-cache"
endif

MAKE_DIR:=$(CURDIR)
PYENV_HOME:=${WORKSPACE}/.pyenv

SOURCE_PATH:=${MAKE_DIR}/ComPyLL
INTEGRATION_TESTS_PATH:=${MAKE_DIR}/tests/programs
DOC_PATH=${MAKE_DIR}/doc

.ONESHELL: venv unit-test integration-test pylint tests
.PHONY: clean clean-venv clean-pip integration-test cleanup all

all: doc tests cleanup

cleanup:
	@echo "Cleaning up"
	$(MAKE) clean-venv
	$(MAKE) clean

tests: clean-results pylint coverage

unit-test: venv
	@echo &&\
	echo "+--------------------+" && \
	echo "| Running Unit tests |" && \
	echo "+--------------------+" && \
	. ${PYENV_HOME}/bin/activate && \
	python setup.py nosetests

integration-test: venv
	@echo  && \
	echo "+--------------------------+" && \
	echo "| Runnig integration tests |" && \
	echo "+--------------------------+" && \
	. ${PYENV_HOME}/bin/activate && \
	${INTEGRATION_TESTS_PATH}/runtests.sh

pylint: venv
	@echo &&\
	echo "+----------------+" && \
	echo "| Running PyLint |" && \
	echo "+----------------+" && \
	. ${PYENV_HOME}/bin/activate && \
	pylint --max-line-length=120 --output-format=parseable --reports=y \
		${SOURCE_PATH}/ | tee pylint.out

doc: clean-doc venv
	@echo && \
	echo "+------------------------+" && \
	echo "| Building Documentation |" && \
	echo "+------------------------+" && \
	. ${PYENV_HOME}/bin/activate && \
	python setup.py build_sphinx

coverage: venv unit-test integration-test
	. ${PYENV_HOME}/bin/activate && \
	coverage xml --omit="${PYENV_HOME}/*" --include="${SOURCE_PATH}/*" -o coverage.xml


venv: clean-venv
	@echo ${MSG} && \
	echo "Setting virtualenv at ${PYENV_HOME}" && \
	virtualenv ${PYENV_HOME} --no-site-packages && \
	. ${PYENV_HOME}/bin/activate && \
	echo "VENV $${VIRTUAL_ENV}" && \
	pip install -IU -r requirements.txt --download-cache ${CACHE_DIR}

clean-all: clean-pip clean-venv clean-doc clean-results

clean-venv: clean clean-coverage clean-tests
	@echo "Cleaning up remains of previous tests..."
	if [ -d ${PYENV_HOME} ]; then \
		rm -rf ${PYENV_HOME}; \
	fi
	rm -rf .nose-stopwatch-times .noseids Python_to_LLVM_Compiler.egg-info

clean-coverage:
	coverage erase

clean-tests:
	rm -rf compiled errors $$SOURCE_PATH/runtime.o

clean-pip:
	@echo "Cleaning pip cache:"
	rm -rf ${CACHE_DIR}

clean-doc:
	@echo "Cleaning documentation:"
	$(MAKE) -C ${DOC_PATH}/ clean
	cd ${MAKE_DIR}

clean:
	@echo "Cleaning pyc files:"
	find . -type f -name "*.pyc" -delete;

clean-results:
	rm -rf errors compiled xunit.xml xunit-integration.xml coverage.xml \
		coverage-it.xml pylint.out $$SOURCE_PATH/runtime.o
