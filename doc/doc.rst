Documentation
#############
   	Official Documentation  (Work in progress):

Available methods
*****************

.. autosummary::
        ComPyLL.compile
        ComPyLL.flatten
        ComPyLL.generate_code
        ComPyLL.ast

Details
*******

.. automodule:: ComPyLL
	:members:
	:undoc-members:
	:show-inheritance:
