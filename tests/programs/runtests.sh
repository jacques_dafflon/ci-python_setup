#!/bin/bash

PKG="ComPyLL"
OLD_PATH="$PWD"
TESTS_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" 
#Go to the test directory
cd $( dirname "${BASH_SOURCE[0]}" )
cd ../..

FAILURES=0
ERRORS=0
PASSED=0
TOTAL=0

TESTS=`ls $TESTS_PATH/*.py`

mkdir -p compiled
mkdir -p errors

echo "Generating runtime lib"
gcc -c $PKG/runtime.c -o $PKG/runtime.o

XML="xunit-integration.xml"
printf "" > $XML

TESTSUITE_START=$(python -c'import time; print repr(time.time())')
for TEST_FILE in $TESTS
do
    TEST_START=$(python -c'import time; print repr(time.time())')
    TEST=$(basename $TEST_FILE)
    TEST=${TEST%.*}
    TOTAL=$((TOTAL+1))
    NEW_LINE=false # For formatting
    printf "Running $TEST... "
    TEST_XML=""

    OUT_PY=`yes 42 2>/dev/null | python $TEST_FILE 2> errors/$TEST.ref.log`
    RET_PY=$?

    OUT_COMPILE=`coverage run --append --branch --source=$PKG $PKG/compile.py $TEST_FILE \
            2> errors/$TEST.act.log > compiled/$TEST.ll`
    RET_COMPILE=$?
    
    ERRORS_REF=$(<errors/$TEST.ref.log)

    if [[ ( $RET_COMPILE == 0 ) ]];
    then
        OUT_LLC=`llc compiled/$TEST.ll -filetype=obj -o compiled/$TEST.o 2>> errors/$TEST.act.log`
        RET_LLC=$?
    else
        OUT_LLC="ERROR: The file ${TEST}.ll was not generated."
        RET_LLC=1
    fi
    
    if [[ ( $RET_LLC == 0 ) ]];
    then
        OUT_LINK=`gcc compiled/$TEST.o $PKG/runtime.o -o compiled/$TEST 2>> errors/$TEST.act.log`
        RET_LINK=$?
    else
        OUT_LINK="ERROR: The executable $TEST was not generated."
        RET_LINK=1
    fi
    
    if [[ ( $RET_LLC == 0 ) ]];
    then
        OUT_RUN=`yes 42 2>/dev/null | compiled/$TEST 2>> errors/$TEST.act.log`
        RET_RUN=$?
    else
        OUT_RUN="ERROR: The executable $TEST was not run."
        RET_RUN=1
    fi
    
    OUTPUT=`echo "$OUT_COMPILE\n$OUT_LLC\n$OUT_LINK\nOUT_RUN"` 

    ERRORS_ACTUAL=$(<errors/$TEST.act.log)
    ERRORS_ACTUAL_XML=$(sed -e 's~&~\&amp;~g' -e 's~<~\&lt;~g'  -e  's~>~\&gt;~g' < errors/$TEST.act.log)

    if [[ ( $RET_PY > 0 && $RET_RUN > 0 ) ]] || \
       [[ $TEST == warn_* && ! -z $ERRORS_ACTUAL && \
              $ERRORS_ACTUAL == WARNING* && $RET_PY == 0 && \
              $RET_RUN == 0 ]] || \
       [[ ( $OUT_PY == $OUT_RUN && $RET_PY == 0 && $RET_RUN == 0 && \
                $RET_RUN == 0 ) ]] || \
       [[ ( $RET_PY == 0 && $RET_RUN > 0 && $TEST == fail_* ) ]];
    then
        PASSED=$((PASSED+1))
        if [[ ( $RET_RUN == 1) ]] || [[ $TEST == warn_* ]];
        then
        echo ""
            echo "--- Expected output: ---"
            if [[ ${#OUTPUT} -gt 3 ]]; then
                    echo "(No output)"
            else
                    echo -e "$OUTPUT"
            fi
            echo "--- Expected errors: ---"
            if [[ -z $ERRORS_ACTUAL ]]; then
                    echo "(No errors)"
            else
                    echo "$ERRORS_ACTUAL"
            fi

            printf " ---\nRan $TEST...  -> pass"
            NEW_LINE=true
        else
            printf "  -> pass"
        fi
    else
        NEW_LINE=true
        if [[ $ERRORS_ACTUAL == *"Traceback (most recent call last):"* ]]
        then 
            ERRORS=$((ERRORS+1))
            TEST_XML="\n\t\t<error type=\"$( echo "$ERROR_ACTUAL"| grep -ve "^  \|^Traceback\|^DEBUG\|^INFO\|^WARNING\|^ERROR\|^CRITICAL" | cut -d: -f1 )\">"
            TEST_XML="$TEST_XML\n$ERRORS_ACTUAL_XML\n\t\t</error>\n\t"
            echo "$TEST: ERRORED"
        elif [[ $ERRORS_ACTUAL == "lli:"* || $ERRORS_ACTUAL == *"\nlli:"* ]]
        then 
            ERRORS=$((ERRORS+1))
            TEST_XML="$TEST_XML\n\t\t<error type=\"LLVM Error\" message=\"`grep -e \"lli\" <<< ${ERRORS_ACTUAL_XML} | sed -n 's/^lli:\(.*\): //p'`\">\n$ERRORS_ACTUAL_XML\n\t\t</error>\n\t"
            echo "$TEST: ERRORED (Compiled file is incorrect.)"
        else
            FAILURES=$((FAILURES+1))
            TEST_XML="$TEST_XML\n\t\t<failure type=\"$ERRORS_ACTUAL_XML\">\n$ERRORS_ACTUAL_XML\n\t\t</failure>\n\t"
            echo "$TEST: FAILED"
        fi
    echo "--- Reference output: ---"
        echo $OUT_PY
        echo "--- Reference errors: ---"
        echo $ERRORS_REF
        echo "---- Actual output: ----"
        echo $OUT_RUN
        echo "---- Actual errors: ----"
        echo $ERRORS_ACTUAL
    fi
    TEST_END=$(python -c'import time; print repr(time.time())')
    TEST_TIME=$(echo "$TEST_END - $TEST_START" | bc)
    TEST_XML="\t<testcase name=\"$TEST\" classname=\"$TEST_FILE\" time=\"$TEST_TIME\">$TEST_XML</testcase>"
    echo -e "$TEST_XML" >> $XML
    printf "  (${TEST_TIME}s)\n"
    if $NEW_LINE ; then #Formatting
            echo ""
    fi
done
echo "--- Result: ---"
echo "$PASSED test of $TOTAL passed"

TESTSUITE_END=$(python -c'import time; print repr(time.time())')
TESTSUITE_TIME=$(echo "$TESTSUITE_END - $TESTSUITE_START" | bc)
echo "Duration: ${TESTSUITE_TIME} seconds."

echo -e "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<testsuite name=\"$PKG Integration Tests\" timestamp=$(date +\"%Y-%m-%dT%H:%M:%S\") hostname=\"$( hostname )\" tests=\"$TOTAL\" failures=\"$FAILURES\" errors=\"$ERRORS\" time=\"$TESTSUITE_TIME\">" | cat - $XML > temp && mv temp $XML
# if [[ -f "$XML-e" ]]; then
# 	echo "Clean up"
# 	rm "$XML-e"
#fi
echo "</testsuite>" >> $XML
echo
echo "------------------->>> Coverage Details <<<-------------------"
#echo "          (potentialy including unit tests coverage)          "
#echo "--------------------------------------------------------------"
coverage report --include="$PKG/*"

cd "$OLD_PATH"

if [[ $PASSED == $TOTAL ]]
then
    echo "TESTS: SUCCESS"
else
    echo "TESTS: FAILURE"
fi

exit 0
