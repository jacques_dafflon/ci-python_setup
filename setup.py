import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name = "Python to LLVM Compiler",
    version = "0.2",
    packages = ['ComPyLL'],
    author = "Jacques Dafflon, Giorgio Gori, Giovanni Viviani",
    author_email = "jacques.dafflon@usi.ch, gorig@usi.ch, vivianig@usi.ch",
    description = "Compiler from Python to LLVM, written in Python.",
    long_description = read("README"),
    license = "MIT License",
    url = "http://www.inf.usi.ch/faculty/nystrom/teaching/compilers/fa13/",
    setup_requires=['nose>=1.0'],
    test_suite = 'nose.collector',
    classifiers = [
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Education"
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 2.7",
        "Operating System :: OS Independent",
        "Topic :: Education",
        "Topic :: Utilities",
        "Topic :: Software Development :: Compilers",
        ],
)
